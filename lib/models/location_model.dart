import 'package:cloud_firestore/cloud_firestore.dart';

class LocationModel {
  String? address;
  double? latitude;
  double? longitude;

  String? userId;

  LocationModel(
      {required this.address, required this.latitude, required this.longitude});

  LocationModel.fromJson(Map<String, dynamic> json) {
    address = json['address'];
    longitude = json['longitude'];
    latitude = json['latitude'];
  }

  Map<String, dynamic> toMap() {
    return {'address': address, 'longitude': longitude, 'latitude': latitude};
  }

  LocationModel.fromGeoPoint(Map<String, dynamic> json) {
    address = json['address'];
    longitude = (json['location'] as GeoPoint).longitude;
    latitude = (json['location'] as GeoPoint).latitude;
    userId = json['userId'];
  }

  Map<String, dynamic> toGeoPointMap() {
    return {
      'address': address,
      'location': GeoPoint(latitude!, longitude!),
      'userId': userId
    };
  }
}
