class PaymentModel {
  String? id;
  double? amountPaid;
  String? rideId;

  PaymentModel({this.id, this.amountPaid = 0, this.rideId});

  PaymentModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    amountPaid = json['amountPaid'];
    rideId = json['rideId'];
  }

  Map<String, dynamic> toMap() {
    return {'id': id, 'rideId': rideId, 'amountPaid': amountPaid};
  }
}
