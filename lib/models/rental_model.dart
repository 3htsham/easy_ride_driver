class RentalModel {
  String? id;
  String? title;
  String? userId;
  String? description;
  String? rentPerDay;
  String? imgUrl;
  String? contact;

  bool isNewImg = false;
  String? newImgPath;
  String? newImgName;

  RentalModel(
      {this.userId = ' ',
      this.id = '',
      this.title = ' ',
      this.description = ' ',
      this.rentPerDay,
      this.imgUrl,
      this.isNewImg = false});

  RentalModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    userId = json['userId'];
    description = json['description'];
    imgUrl = json['imgUrl'];
    rentPerDay = json['rentPerDay'];
    contact = json['contact'];
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'userId': userId,
      'description': description,
      'imgUrl': imgUrl,
      'rentPerDay': rentPerDay,
      'contact': contact,
    };
  }
}
