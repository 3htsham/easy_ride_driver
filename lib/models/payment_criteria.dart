class PayCriteria {
  String? currency;
  int? payPerKm;

  PayCriteria({this.currency, this.payPerKm});

  PayCriteria.fromJson(Map<String, dynamic> json) {
    currency = json['currency'];
    payPerKm = json['pay_per_km'];
  }

  Map<String, dynamic> toMap() {
    return {'currency': currency, 'pay_per_km': payPerKm};
  }
}
