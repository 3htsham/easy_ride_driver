import 'package:flutter/material.dart';

import 'configs.dart';

class AppTypo {
  static TextTheme getAppTextTheme(BuildContext context, {bool isDark = false}) => TextTheme(
    headline1: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 22.0 * MediaQuery.textScaleFactorOf(context),
      color: AppTheme().accentColor(1)
    ),
    headline2: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 24.0 * MediaQuery.textScaleFactorOf(context),
    ),
    headline3: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 22.0 * MediaQuery.textScaleFactorOf(context),
        color: AppTheme().accentColor(1)
    ),
    headline4: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 20.0 * MediaQuery.textScaleFactorOf(context),
    ),
    headline5: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 16.0 * MediaQuery.textScaleFactorOf(context),
        color: AppTheme().accentColor(1)
    ),
    headline6: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 16.0 * MediaQuery.textScaleFactorOf(context),
    ),
    subtitle1: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 18.0 * MediaQuery.textScaleFactorOf(context),
      fontWeight: FontWeight.w800,
        color: AppTheme().accentColor(1)
    ),
    subtitle2: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 15.0 * MediaQuery.textScaleFactorOf(context),
      fontWeight: FontWeight.w800,
    ),
    bodyText1: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 15.0 * MediaQuery.textScaleFactorOf(context),
    ),
    bodyText2: mainAppTextStyle(isDark: isDark).copyWith(
      fontSize: 14.0 * MediaQuery.textScaleFactorOf(context),
    ),
    caption: mainAppTextStyle(isDark: isDark).copyWith(
        fontSize: 12.0 * MediaQuery.textScaleFactorOf(context),
        fontWeight: FontWeight.w500,
        color: isDark ? AppTheme().captionDarkColor(1) : AppTheme().captionColor(1)),
  );

  static TextStyle mainAppTextStyle({bool isDark = false}) => TextStyle(
    // fontFamily: 'Roboto',
      fontWeight: FontWeight.w400,
      color: isDark ? AppTheme().bodyTextDarkColor(1) : AppTheme().bodyTextColor(1));
}