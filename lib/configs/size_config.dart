import 'package:flutter/material.dart';

class SizeConfig {
  BuildContext context;
  static double? height;
  static double? width;
  static double? _heightPadding;
  static double? _widthPadding;

  static double? screenHeight;
  static double? screenWidth;

  SizeConfig(this.context);

  void init() {
    MediaQueryData queryData = MediaQuery.of(context);

    height = queryData.size.height / 100.0;
    width = queryData.size.width / 100.0;

    screenHeight = queryData.size.height;
    screenWidth = queryData.size.width;

    _heightPadding = height! - ((queryData.padding.top + queryData.padding.bottom) / 100.0);
    _widthPadding = width! - (queryData.padding.left + queryData.padding.right) / 100.0;
  }

  static double responsiveHeight(double height) {
    return (height / 800) * SizeConfig.screenHeight!;
  }

  static double responsiveWidth(double width) {
    return (width / 1440) * SizeConfig.screenWidth!;
  }

  static double appHeight(double v) {
    return height! * v;
  }

  static double appWidth(double v) {
    return width! * v;
  }

  static double appVerticalPadding(double v) {
    return _heightPadding! * v;
  }

  static double appHorizontalPadding(double v) {
    return _widthPadding! * v;
  }

  static double appDefaultHorizontalPadding([double v = 9.028]) {
    return _widthPadding! * v;
  }
  static double appDefaultVerticalPadding([double v = 7.40074]) {
    return _heightPadding! * v;
  }
}