import 'package:easy_ride_driver/routes/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../configs/configs.dart';
import '../../models/models.dart';
import '../../services/services.dart';

class AvailableForRide extends StatefulWidget {
  const AvailableForRide({Key? key}) : super(key: key);

  @override
  State<AvailableForRide> createState() => _AvailableForRideState();
}

class _AvailableForRideState extends State<AvailableForRide> {
  @override
  void initState() {
    super.initState();
    listenForRides();
  }

  listenForRides() {
    if (mounted) {
      context.read<RideService>().listenNormalRides();
      context.read<RideService>().listenShareableRides();
    }
  }

  @override
  Widget build(BuildContext context) {
    final ridesAvailable = context.watch<RideService>().availableRides;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Rides Available'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.responsiveWidth(80),
              vertical: SizeConfig.responsiveHeight(10)),
          child: Column(
            children: [
              ridesAvailable.isEmpty
                  ? Padding(
                      padding:
                          EdgeInsets.only(top: SizeConfig.screenHeight! / 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Please hold on!\nWe will show you available rides\nif any.',
                            style: AppTheme.theme!.textTheme.headline4,
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    )
                  : ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: ridesAvailable.length,
                      itemBuilder: (context, idx) {
                        final ride = ridesAvailable[idx];
                        final payment = context.watch<PaymentService>().criteria!;
                        return ListTile(
                          onTap: () {
                            showRideDetailsDialog(ride);
                          },
                          contentPadding: EdgeInsets.all(0),
                          trailing: Icon(
                            CupertinoIcons.forward,
                            color: AppTheme.theme!.accentColor,
                          ),
                          leading: Text('${payment.currency}\n${(payment.payPerKm!*ride.pickUpDropDistance!).toStringAsFixed(0)}', textAlign: TextAlign.center,),
                          title: Text('From: ${ride.pickup!.address}'),
                          subtitle: Text('To: ${ride.dropOff!.address}'),
                        );
                      },
                    )
            ],
          ),
        ),
      ),
    );
  }

  showRideDetailsDialog(RideDetails details) {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            actions: [
              TextButton(
                  onPressed: () {
                    context.read<RideService>().acceptRide(details).then((value) {
                      Navigator.of(context).pop();
                      Navigator.of(context).pushReplacementNamed(RoutePath.trackRide);
                    });
                  },
                  child: const Text('Accept')),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Ignore')),
            ],
            title: const Text('Ride Details'),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text('From: '),
                Text('${details.pickup!.address}', style: AppTheme.theme!.textTheme.bodyText1,),
                const Divider(),
                const Text('Drop at: '),
                Text('${details.dropOff!.address}', style: AppTheme.theme!.textTheme.bodyText1),
                const Divider(),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text('Distance: '),
                    Text('${(details.distance ?? 00).toStringAsFixed(2)} KMs', style: AppTheme.theme!.textTheme.bodyText1),
                  ],
                ),
                const Divider(),
              ],
            ),
          );
        });
  }
}
