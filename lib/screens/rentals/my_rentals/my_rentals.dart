import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_ride_driver/configs/configs.dart';
import 'package:easy_ride_driver/models/models.dart';
import 'package:easy_ride_driver/routes/routes.dart';
import 'package:easy_ride_driver/screens/screens.dart';
import 'package:easy_ride_driver/services/services.dart';
import 'package:easy_ride_driver/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyRentals extends StatefulWidget {
  const MyRentals({Key? key}) : super(key: key);

  @override
  State<MyRentals> createState() => _MyRentalsState();
}

class _MyRentalsState extends State<MyRentals> {
  @override
  void initState() {
    super.initState();
    context.read<RentalService>().getRentals();
  }

  @override
  Widget build(BuildContext context) {
    final loading = context.watch<RentalService>().isLoading;
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Rentals'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.responsiveWidth(25),
              vertical: SizeConfig.responsiveHeight(15)),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'Rentals',
                    style: AppTheme.theme!.textTheme.headline4,
                  ),
                  const Spacer(),
                  SizedBox(
                    width: 100,
                    child: MySolidButton(
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed(RoutePath.addRental)
                            .then((value) {
                          if (value != null) {
                            if (value == true) {
                              context.read<RentalService>().getRentals();
                            }
                          }
                        });
                      },
                      title: '+ Add New',
                    ),
                  ),
                ],
              ),
              const Divider(),
              loading
                  ? SizedBox(
                      height: 100,
                      width: 40,
                      child: Center(
                        child: CircularProgressIndicator(
                          color: AppTheme.theme!.accentColor,
                        ),
                      ),
                    )
                  : context.watch<RentalService>().rentals.isEmpty
                      ? const Padding(
                          padding: EdgeInsets.only(top: 40),
                          child: Text('Nothing to show'),
                        )
                      // : ListView.builder(
                      //     shrinkWrap: true,
                      //     primary: false,
                      //     itemCount:
                      //         context.watch<RentalService>().rentals.length,
                      //     itemBuilder: (ctx, idx) {
                      //       final rental =
                      //           context.watch<RentalService>().rentals[idx];
                      //       return _buildRentalItem(rental);
                      //     },
                      //   )
                      : Wrap(
                          direction: Axis.horizontal,
                          spacing: SizeConfig.responsiveWidth(30),
                          runSpacing: SizeConfig.responsiveWidth(30),
                          runAlignment: WrapAlignment.start,
                          alignment: WrapAlignment.start,
                          children: List.generate(
                              context.watch<RentalService>().rentals.length,
                              (idx) {
                            final rental =
                                context.watch<RentalService>().rentals[idx];
                            return _buildRentalItem(rental);
                          }),
                        )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildRentalItem(RentalModel rental) {
    return InkWell(
      onTap: () {
        //Prompt to Edit or Delete
        _showDetailsDialog(rental);
      },
      child: SizedBox(
        width: (SizeConfig.screenWidth! / 2) -
            (SizeConfig.responsiveWidth(30) * 3),
        height: SizeConfig.responsiveHeight(200),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: SizedBox(
                    width: (SizeConfig.screenWidth! / 2) -
                        (SizeConfig.responsiveWidth(25) * 3),
                    child: CachedNetworkImage(
                      imageUrl: rental.imgUrl!,
                      fit: BoxFit.cover,
                    )),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              '${rental.title}',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }

  _showDetailsDialog(RentalModel rental) {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (ctx) => AddRental(
                                  rentalModel: rental,
                                )))
                        .then((value) {
                      if (value != null) {
                        if (value == true) {
                          context.read<RentalService>().getRentals();
                        }
                      }
                    });
                  },
                  child: const Text('Edit')),
              TextButton(
                  onPressed: () {
                    _showDeleteDialog(rental);
                  },
                  child: const Text('Delete')),
            ],
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                CachedNetworkImage(
                  imageUrl: rental.imgUrl!,
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  height: SizeConfig.responsiveHeight(20),
                ),
                Text(
                  rental.title ?? ' ',
                  style: AppTheme.theme!.textTheme.headline5,
                ),
                SizedBox(
                  height: SizeConfig.responsiveHeight(20),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(CupertinoIcons.money_dollar, color: AppTheme.theme!.accentColor, size: 14,),
                    const SizedBox(width: 10,),
                    Text(
                      'Rent Per Day: ${rental.rentPerDay ?? ' '} ${context.watch<PaymentService>().criteria?.currency ?? ' '}',
                      style: AppTheme.theme!.textTheme.bodyText1,
                    ),
                  ],
                ),
                const Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(CupertinoIcons.phone, color: AppTheme.theme!.accentColor, size: 14,),
                    const SizedBox(width: 10,),
                    Text(
                      'Contact: ${rental.contact ?? ' '}',
                      style: AppTheme.theme!.textTheme.bodyText1,
                    ),
                  ],
                ),
                const Divider(),
                Text(
                  'Details:\n\n${rental.description ?? ' '}',
                  style: AppTheme.theme!.textTheme.bodyText2,
                ),
              ],
            ),
          );
        });
  }

  _showDeleteDialog(RentalModel rental) {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                    context.read<RentalService>().deleteRental(rental);
                  },
                  child: const Text('Yes')),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: const Text('No')),
            ],
            content: Text(
              'Are you sure you want to delete this?',
              style: AppTheme.theme!.textTheme.bodyText1,
            ),
          );
        });
  }
}
