import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_ride_driver/configs/configs.dart';
import 'package:easy_ride_driver/models/models.dart';
import 'package:easy_ride_driver/services/services.dart';
import 'package:easy_ride_driver/widgets/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class AddRental extends StatefulWidget {
  final RentalModel? rentalModel;

  const AddRental({Key? key, this.rentalModel}) : super(key: key);

  @override
  State<AddRental> createState() => _AddRentalState();
}

class _AddRentalState extends State<AddRental> {
  RentalModel rentalModel = RentalModel();
  GlobalKey<FormState> formKey = GlobalKey();

  @override
  void initState() {
    if (widget.rentalModel != null) {
      rentalModel = widget.rentalModel!;
    }
    super.initState();
  }

  validateForm() async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (formKey.currentState?.validate() ?? false) {
      formKey.currentState?.save();
      if (widget.rentalModel != null) {
        await context.read<RentalService>().updateRental(rentalModel);
        if(mounted) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text('Updated Successfully')));
          Navigator.of(context).pop(true);
        }
      } else {
        if (rentalModel.isNewImg) {
          await context.read<RentalService>().addRental(rentalModel);
          if(mounted) {
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text('Added Successfully')));
            Navigator.of(context).pop(true);
          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text('You must add an image of what you are adding')));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final loading = context.watch<RentalService>().isLoading;
    return WillPopScope(
      onWillPop: () async {
        if (loading) {
          return false;
        }
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('My Rentals'),
          leading: IconButton(
            onPressed: () {
              if (!loading) {
                Navigator.of(context).pop();
              }
            },
            icon: const Icon(CupertinoIcons.back),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.responsiveWidth(20),
                vertical: SizeConfig.responsiveHeight(10)),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      _pickImage();
                    },
                    child: Container(
                      width: SizeConfig.screenWidth,
                      height: SizeConfig.screenWidth! * 0.526,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(width: 1, color: Colors.black)),
                      child: rentalModel.isNewImg
                          ? Image.file(File(rentalModel.newImgPath!))
                          : rentalModel.imgUrl != null
                              ? CachedNetworkImage(
                                  imageUrl: rentalModel.imgUrl!,
                                )
                              : const Center(
                                  child: Icon(
                                    CupertinoIcons.photo_fill,
                                    color: Colors.grey,
                                    size: 36,
                                  ),
                                ),
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(20),
                  ),
                  buildTextField(
                      label: 'Title',
                      inputType: TextInputType.text,
                      initialValue: rentalModel.title,
                      validator: (value) => value != null && value.length > 3
                          ? null
                          : 'Enter a valid title',
                      onSave: (value) {
                        rentalModel.title = value;
                      }),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(20),
                  ),
                  buildTextField(
                      label: 'Rent per day',
                      inputType: TextInputType.number,
                      initialValue: rentalModel.rentPerDay,
                      maxLines: 1,
                      validator: (value) => value != null && value.length > 1
                          ? null
                          : 'Enter a valid rent price',
                      formatters: [
                        FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                      ],
                      onSave: (value) {
                        rentalModel.rentPerDay = value;
                      }),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(20),
                  ),
                  buildTextField(
                      label: 'Contact No.',
                      inputType: TextInputType.phone,
                      initialValue: rentalModel.contact,
                      maxLines: 1,
                      validator: (value) => value != null && value.length > 7
                          ? null
                          : 'Enter a valid contact',
                      formatters: [
                        FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                      ],
                      onSave: (value) {
                        rentalModel.contact = value;
                      }),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(20),
                  ),
                  buildTextField(
                      label: 'Description',
                      initialValue: rentalModel.description,
                      inputType: TextInputType.multiline,
                      maxLines: 5,
                      validator: (value) => value != null && value.length > 3
                          ? null
                          : 'Enter a valid description',
                      onSave: (value) {
                        rentalModel.description = value;
                      }),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(20),
                  ),
                  loading
                      ? SizedBox(
                          height: 40,
                          width: 40,
                          child: Center(
                            child: CircularProgressIndicator(
                              color: AppTheme.theme!.accentColor,
                            ),
                          ),
                        )
                      : MySolidButton(
                          onTap: () {
                            validateForm();
                          },
                          title: widget.rentalModel != null
                              ? 'Update'
                              : 'Add Rental',
                        )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  buildTextField(
      {required String? Function(String?) validator,
      required String label,
      required Function(dynamic) onSave,
      required TextInputType inputType,
      String? initialValue,
      int maxLines = 1,
      List<TextInputFormatter>? formatters}) {
    return TextFormField(
      validator: validator,
      keyboardType: inputType,
      initialValue: initialValue,
      maxLines: maxLines,
      style: AppTheme.theme!.textTheme.bodyText1,
      inputFormatters: formatters,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        label: Text(label),
        alignLabelWithHint: true,
        labelStyle: AppTheme.theme!.textTheme.caption,
        border: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: AppTheme.theme!.accentColor)),
        focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: AppTheme.theme!.accentColor)),
        enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: AppTheme.theme!.accentColor)),
      ),
      onSaved: onSave,
    );
  }

  _pickImage() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.camera);
    if (image != null) {
      setState(() {
        rentalModel.isNewImg = true;
        rentalModel.newImgPath = image.path;
        rentalModel.newImgName = image.name;
      });
    }
  }
}
