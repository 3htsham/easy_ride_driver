import 'package:easy_ride_driver/configs/configs.dart';
import 'package:easy_ride_driver/models/models.dart';
import 'package:easy_ride_driver/routes/routes.dart';
import 'package:easy_ride_driver/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../widgets/widgets.dart';

import '../../../services/services.dart';
import '../../../utils/utils.dart';

class EndRideScreen extends StatefulWidget {
  const EndRideScreen({Key? key}) : super(key: key);

  @override
  State<EndRideScreen> createState() => _EndRideScreenState();
}

class _EndRideScreenState extends State<EndRideScreen> {
  RideService? service;

  bool isLoading = true;
  bool loadingPayment = false;

  String currency = ' ';

  double pay1 = 0.0;
  MyUser? user1;
  double pay2 = 0.0;
  MyUser? user2;
  double totalPayment = 0.0;

  @override
  void initState() {
    super.initState();
    setServiceListener();
    calculateFare();
    getUsers();
  }

  @override
  dispose() {
    service?.removeListener(listener);
    super.dispose();
  }

  setServiceListener() {
    service = context.read<RideService>();
    service?.addListener(listener);
  }

  listener() {}

  calculateFare() {
    final payCr = context.read<PaymentService>().criteria!;
    final service = context.read<RideService>();
    final ride = service.currentRide!;

    currency = payCr.currency ?? ' ';

    pay1 = CalUtils.calculatePayment(
        ride, payCr.payPerKm?.toDouble() ?? 0.0, PayType.p1);
    pay2 = CalUtils.calculatePayment(
        ride, payCr.payPerKm?.toDouble() ?? 0.0, PayType.p2);
    totalPayment = pay1 + pay2;

    setState(() {});
  }

  getUsers() async {
    final authService = context.read<AuthService>();
    final service = context.read<RideService>();
    final ride = service.currentRide!;
    final p1 = ride.pickup;
    final p2 = ride.pickupTwo;
    user1 = await authService.getUserDetailsById(p1!.userId!);
    if (p2 != null) {
      user2 = await authService.getUserDetailsById(p2.userId!);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Payment Details',
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: isLoading
            ? Padding(
                padding: EdgeInsets.symmetric(
                    vertical: SizeConfig.responsiveHeight(100)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        CircularProgressIndicator(
                          color: AppTheme.theme!.accentColor,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Text(
                          'Please wait...\nWhile we load payment details for you',
                          textAlign: TextAlign.center,
                        )
                      ],
                    )
                  ],
                ),
              )
            : Padding(
                padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.responsiveHeight(10),
                  horizontal: SizeConfig.responsiveWidth(30),
                ),
                child: Column(
                  children: [
                    ListTile(
                      title: Text('${user1!.name}'),
                      subtitle:
                          Text('Fare: ${pay1.toStringAsFixed(1)} $currency'),
                    ),
                    if (user2 != null)
                      ListTile(
                        title: Text('${user2!.name}'),
                        subtitle:
                            Text('Fare: ${pay2.toStringAsFixed(1)} $currency'),
                      ),
                    SizedBox(
                      height: SizeConfig.responsiveHeight(15),
                    ),
                    loadingPayment
                        ? SizedBox(
                            height: 40,
                            width: 40,
                            child: Center(
                              child: CircularProgressIndicator(
                                color: AppTheme.theme!.accentColor,
                              ),
                            ),
                          )
                        : MySolidButton(
                            onTap: () async {
                              if (!loadingPayment) {
                                setState(() {
                                  loadingPayment = true;
                                });
                                final ride =
                                    context.read<RideService>().currentRide!;
                                PaymentModel pay = PaymentModel(
                                    amountPaid: totalPayment, rideId: ride.id);
                                await context.read<RideService>().endRide(ride);
                                if (mounted) {
                                  await context
                                      .read<PaymentService>()
                                      .addPayment(pay);
                                  if (mounted) {
                                    Navigator.of(context)
                                        .pushNamedAndRemoveUntil(
                                            RoutePath.home, (route) => false);
                                  }
                                }
                              }
                            },
                            title: 'Add Payment',
                          )
                  ],
                ),
              ),
      ),
    );
  }
}
