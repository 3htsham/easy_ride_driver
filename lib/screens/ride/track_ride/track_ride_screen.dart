import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride_driver/enums/enums.dart';
import 'package:easy_ride_driver/routes/routes.dart';
import 'package:easy_ride_driver/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:provider/provider.dart';
import 'package:location/location.dart' as l;
import '../../../configs/configs.dart';
import '../../../models/models.dart';
import '../../../services/services.dart';
import '../../../utils/utils.dart';

class TrackRideScreen extends StatefulWidget {
  const TrackRideScreen({Key? key}) : super(key: key);

  @override
  State<TrackRideScreen> createState() => _TrackRideScreenState();
}

class _TrackRideScreenState extends State<TrackRideScreen> {
  List<Marker> allMarkers = [];
  GoogleMapController? _mapController;
  LocationModel? selectedLoc;

  TextEditingController pickupController = TextEditingController();
  TextEditingController dropController = TextEditingController();

  String driver = 'driver';
  String pick1 = 'pick1';
  String pick2 = 'pick2';
  String drop = 'drop';

  RideService? service;

  @override
  void initState() {
    super.initState();
    setServiceListener();
    setLocationListener();
  }

  @override
  dispose() {
    service?.removeListener(listener);
    super.dispose();
  }

  setServiceListener(){
    service = context.read<RideService>();
    service?.addListener(listener);
  }

  listener() async {
    final ride = context.read<RideService>().currentRide!;
    final pick1 = ride.pickup;
    final pick2 = ride.pickupTwo;
    final drop = ride.dropOff!;
    dropController.text = '${drop.address}';
    if ((pick1 != null && pick2 == null) &&
        ride.target == RideTarget.pickOne.status) {
      pickupController.text = '${pick1.address}';
      addMarker(
          l.LocationData.fromMap({
            'latitude': pick1.latitude,
            'longitude': pick1.longitude
          }),
          this.pick1,
          'Pickup location');
    }
    if (pick2 != null &&
        ride.target == RideTarget.pickTwo.status) {
      pickupController.text = '${pick2.address}';
      removeMarker(this.pick1);
      addMarker(
          l.LocationData.fromMap({
            'latitude': pick2.latitude,
            'longitude': pick2.longitude
          }),
          this.pick2,
          'Next Pickup location');
    } else {
      removeMarker(this.pick1);
      removeMarker(this.pick2);
      addMarker(
          l.LocationData.fromMap({
            'latitude': drop.latitude,
            'longitude': drop.longitude
          }),
          this.drop,
          'Destination');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RideService>(
      builder: (ctx, service, child) {
        final ride = service.currentRide!;
        double distance = CalUtils.calculateDistance(ride.driverLocation!.latitude,
            ride.driverLocation!.longitude,
            ride.pickup!.latitude!, ride.pickup!.longitude!);

        if(ride.target == RideTarget.pickTwo.status) {
          distance = CalUtils.calculateDistance(ride.driverLocation!.latitude,
              ride.driverLocation!.longitude,
              ride.pickupTwo!.latitude!, ride.pickupTwo!.longitude!);
        }

        final destDistance =  CalUtils.calculateDistance(ride.driverLocation!.latitude,
            ride.driverLocation!.longitude,
            ride.dropOff!.latitude!, ride.dropOff!.longitude!);

        return Scaffold(
          appBar: AppBar(
            title: const Text(
              'EasyRide Route',
            ),
            leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: const Icon(CupertinoIcons.back),
            ),
          ),
          body: Stack(
            children: [
              SizedBox(
                  height: SizeConfig.screenHeight,
                  width: SizeConfig.screenWidth,
                  child: GoogleMap(
                    onMapCreated: (GoogleMapController controller) {
                      _mapController = controller;
                      final ride = context.read<RideService>().currentRide!;
                      final pick1 = ride.pickup;
                      final pick2 = ride.pickupTwo;
                      final drop = ride.dropOff!;
                      dropController.text = '${drop.address}';
                      if ((pick1 != null && pick2 == null) &&
                          ride.target == RideTarget.pickOne.status) {
                        pickupController.text = '${pick1.address}';
                        addMarker(
                            l.LocationData.fromMap({
                              'latitude': pick1.latitude,
                              'longitude': pick1.longitude
                            }),
                            this.pick1,
                            'Pickup location');
                      }
                      if (pick2 != null &&
                          ride.target == RideTarget.pickTwo.status) {
                        pickupController.text = '${pick2.address}';
                        removeMarker(this.pick1);
                        addMarker(
                            l.LocationData.fromMap({
                              'latitude': pick2.latitude,
                              'longitude': pick2.longitude
                            }),
                            this.pick2,
                            'Next Pickup location');
                      } else {
                        removeMarker(this.pick1);
                        removeMarker(this.pick2);
                        addMarker(
                            l.LocationData.fromMap({
                              'latitude': drop.latitude,
                              'longitude': drop.longitude
                            }),
                            this.drop,
                            'Destination');
                      }
                    },
                    initialCameraPosition: const CameraPosition(
                        target: LatLng(33.6780513, 73.1843443), zoom: 12),
                    onTap: (pos) {},
                    markers: Set.from(allMarkers),
                    zoomControlsEnabled: false,
                  )),
              Positioned(
                bottom: 0,
                child: Container(
                  decoration: StyleUtils.getContainerShadowDecoration(),
                  padding: StyleUtils.getDefaultPadding(),
                  width: SizeConfig.screenWidth,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'EasyRide Details',
                            style: AppTheme.theme!.textTheme.caption
                                ?.copyWith(color: Colors.black45),
                          )
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.responsiveHeight(15),
                      ),
                      _buildTextField(
                          controller: pickupController,
                          label: 'Next Pickup',
                          icon: CupertinoIcons.location_solid,
                          onTap: () {}),
                      SizedBox(
                        height: SizeConfig.responsiveHeight(10),
                      ),
                      _buildTextField(
                          controller: dropController,
                          label: 'Destination',
                          icon: CupertinoIcons.location_solid,
                          onTap: () {}),

                      SizedBox(
                        height: SizeConfig.responsiveHeight(10),
                      ),

                      if(distance < 1 && ride.rideStatus == RideStatus.driverFound.status)
                        MySolidButton(
                          onTap: (){
                            //Change Ride Status To Reached
                            context.read<RideService>().driverReached(ride);
                          },
                          title: 'Reached',
                        ),

                      if(distance < 1 && ride.rideStatus == RideStatus.started.status && ride.target == RideTarget.pickTwo.status)
                        MySolidButton(
                          onTap: (){
                            //Change Ride To Reached to 2nd Point, Target change to DropOff
                            context.read<RideService>().driverReachedTo2nd(ride);
                          },
                          title: 'Reached',
                        ),

                      if(ride.rideStatus == RideStatus.driverReached.status)
                        MySolidButton(
                          onTap: (){
                            //Start the ride
                            context.read<RideService>().startRide(ride);
                          },
                          title: 'Start Ride',
                        ),

                      if(destDistance < 1 && ride.rideStatus == RideStatus.started.status)
                        MySolidButton(
                          onTap: (){
                            //Navigate To Payment Page, with mentioning both rider's payments
                            Navigator.of(context).pushNamed(RoutePath.endRide);
                          },
                          title: 'End Ride',
                        ),

                    ],
                  ),
                ),
              )
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              _launchMap();
            },
            child: const Icon(CupertinoIcons.location_circle_fill),
          ),
        );
      },
    );
  }

  _launchMap() async {
    final ride = context.read<RideService>().currentRide!;
    final pick1 = ride.pickup;
    final pick2 = ride.pickupTwo;
    final drop = ride.dropOff!;
    if ((pick1 != null && pick2 == null) &&
        ride.target == RideTarget.pickOne.status) {
      //Launch with PickupOne
      MapsLauncher.launchCoordinates(pick1.latitude!, pick1.longitude!);
    } else if (pick2 != null && ride.target == RideTarget.pickTwo.status) {
      //Launch with Pickup two
      MapsLauncher.launchCoordinates(pick2.latitude!, pick2.longitude!);
    } else {
      //Launch with Drop off
      MapsLauncher.launchCoordinates(drop.latitude!, drop.longitude!);
    }
  }

  Widget _buildTextField(
      {required TextEditingController controller,
      required String label,
      required IconData icon,
      required VoidCallback onTap}) {
    return SizedBox(
      height: 35,
      child: InkWell(
        onTap: () {
          onTap.call();
        },
        child: TextFormField(
          controller: controller,
          enabled: false,
          style:
              AppTheme.theme!.textTheme.caption?.copyWith(color: Colors.black),
          decoration: InputDecoration(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              label: Text(label),
              labelStyle: AppTheme.theme!.textTheme.caption
                  ?.copyWith(color: Colors.black),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide:
                    BorderSide(width: 1, color: AppTheme.theme!.accentColor),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide:
                    BorderSide(width: 1, color: AppTheme.theme!.accentColor),
              ),
              prefixIcon: Icon(
                icon,
                size: 18,
                color: AppTheme.theme!.accentColor,
              )),
        ),
      ),
    );
  }

  setLocationListener() async {
    final listener = l.Location.instance.onLocationChanged;
    listener.listen((l.LocationData currentLocation) {
      if (mounted) {
        //GeoPoint
        GeoPoint geoPoint =
            GeoPoint(currentLocation.latitude!, currentLocation.longitude!);
        addMarker(currentLocation, driver, 'You\'re here', moveCam: true);
        //Firebase Update
        context.read<RideService>().updateDriverLocation(
            context.read<RideService>().currentRide!, geoPoint);
      }
    });
  }

  addMarker(l.LocationData loc, String id, String title,
      {bool moveCam = false}) {
    final marker = Marker(
        markerId: MarkerId(id),
        draggable: false,
        infoWindow: InfoWindow(title: title, snippet: ''),
        position: LatLng(loc.latitude!, loc.longitude!));
    allMarkers.removeWhere((element) => element.markerId.value == id);
    setState(() {
      allMarkers.add(marker);
    });
    if (moveCam) {
      moveCamera(loc);
    }
  }

  removeMarker(String id) {
    allMarkers.removeWhere((element) => element.markerId.value == id);
  }

  moveCamera(l.LocationData loc) {
    if (_mapController != null) {
      _mapController!.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(loc.latitude!, loc.longitude!),
            zoom: 15,
          ),
        ),
      );
    }
  }
}
