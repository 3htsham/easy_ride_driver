import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import '../configs/configs.dart';
import '../enums/enums.dart';
import '../models/models.dart';
import '../utils/utils.dart';
import 'services.dart';

class RideService extends ChangeNotifier {
  RideDetails? currentRide;

  List<RideDetails> availableRides = [];

  final uuid = const Uuid();

  BuildContext context;

  RideService(this.context);

  listenShareableRides() async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides =
        fireStore.collection(DatabaseUtil.shareableRides);
    rides
        .where('rideStatus', isEqualTo: RideStatus.searching.status)
        .snapshots()
        .listen((snap) {
      if (snap.docs.isNotEmpty) {
        final docsSnap = snap.docs;
        for (int i = 0; i < docsSnap.length; i++) {
          final doc = docsSnap[i];
          final data = doc.data() as Map<String, dynamic>;
          RideDetails ride = RideDetails.fromJson(data);
          availableRides.removeWhere((element) => element.id == ride.id);
          final myLoc = Application.myLocation;
          ride.distance = CalUtils.calculateDistance(
              myLoc!.latitude,
              myLoc.longitude,
              ride.pickup!.latitude!,
              ride.pickup!.longitude!);
          ride.pickUpDropDistance = CalUtils.calculateDistance(
              ride.dropOff!.latitude!,
              ride.dropOff!.longitude!,
              ride.pickup!.latitude!,
              ride.pickup!.longitude!);
          availableRides.add(ride);
          availableRides.sort((a, b) => a.distance! > b.distance! ? 1 : 0);
          notifyListeners();
        }
      }
      UtilLogger.log(
          'RIDE SERVICE listenShareableRides()', '${availableRides.length}');
    });
  }

  listenNormalRides() async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(DatabaseUtil.rides);
    rides
        .where('rideStatus', isEqualTo: RideStatus.searching.status)
        .snapshots()
        .listen((snap) {
      if (snap.docs.isNotEmpty) {
        final docsSnap = snap.docs;
        for (int i = 0; i < docsSnap.length; i++) {
          final doc = docsSnap[i];
          final data = doc.data() as Map<String, dynamic>;
          RideDetails ride = RideDetails.fromJson(data);
          availableRides.removeWhere((element) => element.id == ride.id);
          final myLoc = Application.myLocation;
          ride.distance = CalUtils.calculateDistance(
              myLoc!.latitude,
              myLoc.longitude,
              ride.pickup!.latitude!,
              ride.pickup!.longitude!);
          ride.pickUpDropDistance = CalUtils.calculateDistance(
              ride.dropOff!.latitude!,
              ride.dropOff!.longitude!,
              ride.pickup!.latitude!,
              ride.pickup!.longitude!);
          availableRides.add(ride);
          availableRides.sort((a, b) => a.distance! > b.distance! ? 1 : 0);
          notifyListeners();
        }
      }
      UtilLogger.log(
          'RIDE SERVICE listenNormalRides()', '${availableRides.length}');
    });
  }

  Future<void> acceptRide(RideDetails details) async {
    //Change Ride Status, assign driverId
    //Change onGoingRide Id in Driver's profile
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    CollectionReference users = fireStore.collection(DatabaseUtil.drivers);
    details.driverId = Application.user!.id;
    details.rideStatus = RideStatus.driverFound.status;
    details.isAccepted = true;
    Application.user!.onGoingRide = details.id;
    await rides.doc(details.id).set(details.toMap(), SetOptions(merge: true));
    await users
        .doc(Application.user!.id)
        .set({'ongoing_ride': details.id}, SetOptions(merge: true));
    UtilLogger.log('RideService', "Ride Accepted");
    listenRide(details);
    return;
  }

  listenRide(RideDetails details) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    rides.doc(details.id).snapshots().listen((snap) {
      if (snap.exists) {
        final data = snap.data();
        if (data != null) {
          currentRide = RideDetails.fromJson(data as Map<String, dynamic>);
          if (!currentRide!.completed) {
            notifyListeners();
          }
        }
      }
    });
  }

  //Pass rideId from user's profile
  checkOnGoingRide(String rideId) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(DatabaseUtil.rides);
    final rideDoc = await rides.doc(rideId).get();
    if (rideDoc.exists) {
      final data = rideDoc.data() as Map<String, dynamic>;
      currentRide = RideDetails.fromJson(data);
      currentRide!.pickUpDropDistance = CalUtils.calculateDistance(
          currentRide!.dropOff!.latitude!,
          currentRide!.dropOff!.longitude!,
          currentRide!.pickup!.longitude!,
          currentRide!.pickup!.longitude!);
    } else {
      CollectionReference shareableRides =
          fireStore.collection(DatabaseUtil.shareableRides);
      final shareableRide = await shareableRides.doc(rideId).get();
      if (shareableRide.exists) {
        final data = shareableRide.data() as Map<String, dynamic>;
        currentRide = RideDetails.fromJson(data);
        currentRide!.pickUpDropDistance = CalUtils.calculateDistance(
            currentRide!.dropOff!.latitude!,
            currentRide!.dropOff!.longitude!,
            currentRide!.pickup!.longitude!,
            currentRide!.pickup!.longitude!);
      }
    }
    notifyListeners();
    if (currentRide != null) {
      listenRide(currentRide!);
    }
  }

  createRide(RideDetails details) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    await rides.doc(details.id).set(details.toMap());
    //Add ride to user's profile as well
    CollectionReference users = fireStore.collection(DatabaseUtil.users);
    await users
        .doc(Application.user!.id)
        .set({'ongoing_ride': details.id}, SetOptions(merge: true));
    UtilLogger.log('RideService', "Ride Created Added");
    return details.id;
  }

  //TODO: Add Payment Details as well
  endRide(RideDetails details) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    Map<String, dynamic> map = {
      'rideStatus': RideStatus.completed.status,
      'target': -1,
    };
    details.rideStatus = RideStatus.completed.status;
    details.target = -1;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    await rides.doc(details.id).set(map, SetOptions(merge: true));
    currentRide = null;
    Application.user!.onGoingRide = null;
    CollectionReference users = fireStore.collection(DatabaseUtil.users);
    CollectionReference drivers = fireStore.collection(DatabaseUtil.drivers);
    await users
        .doc(details.pickup!.userId!)
        .set({'ongoing_ride': null}, SetOptions(merge: true));
    if(details.pickupTwo != null) {
      await users
          .doc(details.pickupTwo!.userId!)
          .set({'ongoing_ride': null}, SetOptions(merge: true));
    }
    await drivers
        .doc(Application.user!.id)
        .set({'ongoing_ride': null}, SetOptions(merge: true));
    notifyListeners();
    return;
  }

  driverReached(RideDetails details) async {
    //Remove ID from user's profile as well
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    rides
        .doc(details.id)
        .set({'rideStatus': RideStatus.driverReached.status}, SetOptions(merge: true));
  }
  driverReachedTo2nd(RideDetails details) async {
    //Remove ID from user's profile as well
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    rides
        .doc(details.id)
        .set({'target': RideTarget.dropOff.status}, SetOptions(merge: true));
  }
  startRide(RideDetails details) async {
    //Remove ID from user's profile as well
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    rides
        .doc(details.id)
        .set({'rideStatus': RideStatus.started.status, 'target': RideTarget.dropOff.status}, SetOptions(merge: true));
  }

  cancelRide(RideDetails details) async {
    //Remove ID from user's profile as well
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    await rides.doc(details.id).delete();
    currentRide = null;
    CollectionReference users = fireStore.collection(DatabaseUtil.users);
    users
        .doc(Application.user!.id)
        .set({'ongoing_ride': null}, SetOptions(merge: true));
    Application.user!.onGoingRide = null;
    notifyListeners();
  }

  updateDriverLocation(RideDetails details, GeoPoint location) async {
    //driverLocation
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    await rides
        .doc(details.id)
        .set({'driverLocation': location}, SetOptions(merge: true));
  }
}
