import 'dart:io';

import 'package:easy_ride_driver/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:uuid/uuid.dart';
import '../configs/configs.dart';
import '../models/models.dart';
import 'services.dart';

class RentalService extends ChangeNotifier {
  bool isLoading = false;
  var uuid = const Uuid();
  List<RentalModel> rentals = [];
  BuildContext context;

  RentalService(this.context);

  addRental(RentalModel rental) async {
    isLoading = true;
    notifyListeners();
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.rentals);
    final storageRef =
        FirebaseStorage.instance.ref().child(DatabaseUtil.storageImages);
    rental.id = uuid.v1();
    rental.userId = Application.user!.id!;
    if (rental.isNewImg) {
      //Update ImageFirst
      final imgRef = storageRef.child(rental.newImgName!);
      await imgRef.putFile(File(rental.newImgPath!));
      rental.imgUrl = await imgRef.getDownloadURL();
    }
    await fireStore.doc(rental.id).set(rental.toMap());
    isLoading = false;
    notifyListeners();
  }

  updateRental(RentalModel rental) async {
    isLoading = true;
    notifyListeners();
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.rentals);
    final storageRef =
        FirebaseStorage.instance.ref().child(DatabaseUtil.storageImages);
    if (rental.isNewImg) {
      //Update ImageFirst
      final imgRef = storageRef.child(rental.newImgName!);
      await imgRef.putFile(File(rental.newImgPath!));
      rental.imgUrl = await imgRef.getDownloadURL();
    }
    await fireStore.doc(rental.id).set(rental.toMap());
    isLoading = false;
    notifyListeners();
  }

  getRentals() async {
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.rentals);
    rentals.clear();
    isLoading = true;
    notifyListeners();
    final querySnap =
        await fireStore.where('userId', isEqualTo: Application.user!.id!).get();
    final docs = querySnap.docs;
    if (docs.isNotEmpty) {
      for (int i = 0; i < docs.length; i++) {
        final doc = docs[i];
        if (doc.exists) {
          final data = doc.data();
          final rentalModel = RentalModel.fromJson(data);
          rentals.add(rentalModel);
          notifyListeners();
        }
      }
    }
    isLoading = false;
    notifyListeners();
  }

  deleteRental(RentalModel rental) async {
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.rentals);
    rentals.clear();
    isLoading = true;
    notifyListeners();
    await fireStore.doc(rental.id).delete();
    getRentals();
  }
}
