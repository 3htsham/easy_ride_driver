import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride_driver/models/payment_model.dart';
import 'package:easy_ride_driver/services/services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import '../models/payment_criteria.dart';
import '../utils/utils.dart';

class PaymentService extends ChangeNotifier {
  final uuid = const Uuid();
  PayCriteria? criteria;

  BuildContext context;

  PaymentService(this.context);

  getPaymentCriteria() async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference config = fireStore.collection(DatabaseUtil.config);
    final payCriteria = await config.doc(DatabaseUtil.paymentCriteria).get();
    final data = payCriteria.data() as Map<String, dynamic>;
    criteria = PayCriteria.fromJson(data);
    notifyListeners();
  }

  addPayment(PaymentModel payment) async {
    payment.id = uuid.v1();
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference pay = fireStore.collection(DatabaseUtil.payments);
    await pay.doc(payment.id).set(payment.toMap());
    return;
  }
}
