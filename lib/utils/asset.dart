class UtilAsset {
  static const String images = "assets/images";
  static const String icons = "assets/icons";

  static const String logoImg = "$icons/logo_icon.jpeg";
  static const String banner = '$images/main_banner.jpeg';
  static const String carIcon = '$icons/car_icon.png';
  static const String cars = '$icons/cars.png';
}
