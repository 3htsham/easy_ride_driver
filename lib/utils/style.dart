import 'package:flutter/material.dart';

import '../configs/configs.dart';

class StyleUtils {

  static BoxDecoration getContainerShadowDecoration({Color? background}) {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(color: AppTheme().accentColor(0.4),
          offset: const Offset(1,1),
          blurRadius: 10
        ),
      ],
      borderRadius: BorderRadius.circular(15),
      color: background ?? const Color(0xffffffff),
    );
  }

  static EdgeInsets getDefaultPadding({Color? background}) {
    return EdgeInsets.symmetric(horizontal: SizeConfig.responsiveWidth(30),
        vertical: SizeConfig.responsiveHeight(20));
  }

}