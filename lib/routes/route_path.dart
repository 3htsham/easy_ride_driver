class RoutePath {
  static const String splash = '/Splash';
  static const String login = '/Login';
  static const String signup = '/SignUp';
  static const String forgotPass = '/ForgotPassword';
  static const String home = '/Home';
  static const String availableForRide = '/AvailableForRide';
  static const String trackRide = '/TrackRide';
  static const String endRide = '/EndRide';

  static const String myRentals = '/MyRentals';
  static const String addRental = '/AddRental';
  static const String editRental = '/EditRental';
}
