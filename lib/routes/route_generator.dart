import 'package:flutter/material.dart';

import '../screens/screens.dart';
import 'route_path.dart';

class RouteGenerator {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case RoutePath.splash:
        return MaterialPageRoute(builder: (context) {
          return const SplashScreen();
        }
            //     builder: (ctx) => ChangeNotifierProvider<HomePageProvider>(
            //   create: (ctx) => HomePageProvider(ctx.read<PrayersService>()),
            //   child: HomePage(),
            // ));
            );
      case RoutePath.login:
        return MaterialPageRoute(builder: (context) {
          return const LoginScreen();
        });
      case RoutePath.signup:
        return MaterialPageRoute(builder: (context) {
          return const SignUpScreen();
        });
      case RoutePath.forgotPass:
        return MaterialPageRoute(builder: (context) {
          return const ForgetPassScreen();
        });
      case RoutePath.home:
        return MaterialPageRoute(builder: (context) {
          return const HomeScreen();
        });
      case RoutePath.availableForRide:
        return MaterialPageRoute(builder: (context) {
          return const AvailableForRide();
        });
      case RoutePath.trackRide:
        return MaterialPageRoute(builder: (context) {
          return const TrackRideScreen();
        });
      case RoutePath.endRide:
        return MaterialPageRoute(builder: (context) {
          return const EndRideScreen();
        });

      case RoutePath.myRentals:
        return MaterialPageRoute(builder: (context) {
          return const MyRentals();
        });
      case RoutePath.addRental:
        return MaterialPageRoute(builder: (context) {
          return const AddRental();
        });
      case RoutePath.editRental:
        return MaterialPageRoute(builder: (context) {
          return const EditRental();
        });
      default:
        return MaterialPageRoute(
          builder: (context) {
            return Scaffold(
              appBar: AppBar(
                title: const Text('Not Found'),
              ),
              body: Center(
                child: Text('No path for ${settings.name}'),
              ),
            );
          },
        );
    }
  }
}
